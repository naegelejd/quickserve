package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
)

func main() {
	port := flag.Int("port", 8080, "port on which to listen")
	flag.Parse()
	var dir string
	if flag.NArg() < 1 {
		dir = "./"
	} else {
		dir = flag.Arg(0)
	}
	addr := fmt.Sprintf(":%d", *port)

	log.Printf("Serving %s on %s\n", dir, addr)
	http.Handle("/", http.FileServer(http.Dir(dir)))
	log.Fatal(http.ListenAndServe(addr, nil))
}
